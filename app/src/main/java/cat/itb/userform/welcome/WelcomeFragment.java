package cat.itb.userform.welcome;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;

public class WelcomeFragment extends Fragment {

    private WelcomeViewModel mViewModel;

    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcome_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(WelcomeViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick({R.id.welcomeLoginButton, R.id.welcomeRegisterButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.welcomeLoginButton:
                Navigation.findNavController(view).navigate(R.id.action_welcomeFragment_to_loginFragment);
                break;
            case R.id.welcomeRegisterButton:
                Navigation.findNavController(view).navigate(R.id.action_welcomeFragment_to_registerFragment);
                break;
        }
    }
}
