package cat.itb.userform.register;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;

public class RegisterFragment extends Fragment {

    @BindView(R.id.username)
    TextInputEditText username;
    @BindView(R.id.password)
    TextInputEditText password;
    @BindView(R.id.repeat_password)
    TextInputEditText repeatPassword;
    @BindView(R.id.email)
    TextInputEditText email;
    @BindView(R.id.name)
    TextInputEditText name;
    @BindView(R.id.surnames)
    TextInputEditText surnames;
    @BindView(R.id.birth_date)
    TextInputEditText birthDate;
    @BindView(R.id.gender)
    TextInputEditText gender;
    @BindView(R.id.check_terms)
    MaterialCheckBox checkTerms;
    @BindView(R.id.usernameLayout)
    TextInputLayout usernameLayout;
    @BindView(R.id.passwordLayout)
    TextInputLayout passwordLayout;
    @BindView(R.id.repeatLayout)
    TextInputLayout repeatLayout;
    @BindView(R.id.emailLayout)
    TextInputLayout emailLayout;
    @BindView(R.id.nameLayout)
    TextInputLayout nameLayout;
    @BindView(R.id.surnamesLayout)
    TextInputLayout surnamesLayout;
    @BindView(R.id.birthLayout)
    TextInputLayout birthLayout;
    @BindView(R.id.genderLayout)
    TextInputLayout genderLayout;
    @BindView(R.id.loginFragmentLoginButton)
    MaterialButton loginFragmentLoginButton;
    @BindView(R.id.registerFragmentRegisterButton)
    MaterialButton loginFragmentRegisterButton;
    @BindView(R.id.welcomeImageView)
    ImageView welcomeImageView;
    private RegisterViewModel mViewModel;
    String[] optionsArray = new String[2];

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick({R.id.loginFragmentLoginButton, R.id.registerFragmentRegisterButton, R.id.birth_date, R.id.gender})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.loginFragmentLoginButton:
                Navigation.findNavController(view).navigate(R.id.action_registerFragment_to_loginFragment);
                break;
            case R.id.registerFragmentRegisterButton:
                register(view);
                break;
            case R.id.birth_date:
                MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
                builder.setTitleText("Birth Date");
                MaterialDatePicker<Long> picker = builder.build();
                picker.addOnPositiveButtonClickListener(this::doOnDateSelected);
                picker.show(getFragmentManager(), picker.toString());
                break;
            case R.id.gender:
                optionsArray[0] = "male";
                optionsArray[1] = "female";
                new MaterialAlertDialogBuilder(getContext())
                        .setTitle("gender")
                        .setItems(optionsArray, this::OnOptionClicked)
                        .show();
                break;
        }
    }

    private void OnOptionClicked(DialogInterface dialogInterface, int i) {
        gender.setText(optionsArray[i]);
    }

    private void register(View view) {
        usernameLayout.setError("");
        passwordLayout.setError("");
        repeatLayout.setError("");
        emailLayout.setError("");
        nameLayout.setError("");
        surnamesLayout.setError("");
        birthLayout.setError("");
        genderLayout.setError("");
        boolean errors = false;
        if (checkEmpty(username)) {
            usernameLayout.setError("Username Required");
            errors = true;
        }
        if (checkEmpty(password)) {
            passwordLayout.setError("Password Required");
            errors = true;
        }

        if (checkEmpty(repeatPassword)) {
            repeatLayout.setError("Repeat Required");
            errors = true;
        } else if (!repeatPassword.getText().toString().equals(password.getText().toString())) {
            repeatLayout.setError("Passwords don't match");
            errors = true;
        }

        if (checkEmpty(email)) {
            emailLayout.setError("Email Required");
            errors = true;
        }
        if (checkEmpty(name)) {
            nameLayout.setError("Name Required");
            errors = true;
        }
        if (checkEmpty(surnames)) {
            surnamesLayout.setError("Surnames Required");
            errors = true;
        }
        if (checkEmpty(birthDate)) {
            birthLayout.setError("Birth date Required");
            errors = true;
        }
        if (checkEmpty(gender)) {
            genderLayout.setError("Gender Required");
            errors = true;
        }
        if (!checkTerms.isChecked()) {
            Snackbar snackbar = Snackbar.make(view, "Accept terms", BaseTransientBottomBar.LENGTH_SHORT);
            snackbar.show();
            errors = true;
        }
        if (!errors)
            Navigation.findNavController(view).navigate(R.id.action_registerFragment_to_userLoggedFragment);
    }

    public boolean checkEmpty(TextInputEditText textInputEditText) {
        if (textInputEditText.getText().toString().isEmpty()) return true;
        else return false;
    }

    private void doOnDateSelected(Long aLong) {
        String dateString = DateFormat.format("MM/dd/yyyy", new Date(aLong)).toString();
        birthDate.setText(dateString);
    }
}
