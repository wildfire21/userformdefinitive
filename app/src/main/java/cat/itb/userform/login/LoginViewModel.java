package cat.itb.userform.login;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.userform.retrofit.UserApi;
import cat.itb.userform.retrofit.UserApiProvider;
import cat.itb.userform.retrofit.UserSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginViewModel extends ViewModel {
    public static final String UNKNOWN_ERROR = "unknown error";
    MutableLiveData<Boolean> logged = new MutableLiveData<>(false);
    MutableLiveData<Boolean> loading = new MutableLiveData<>(false);
    MutableLiveData<String> error = new MutableLiveData<>();

    UserApi userApi = UserApiProvider.getUserApi();

    public void isLogged(String username) {
        loading.setValue(true);
        Call<UserSession> call = userApi.login(username);
        call.enqueue(new Callback<UserSession>() {
            @Override
            public void onResponse(Call<UserSession> call, Response<UserSession> response) {
                UserSession userSession = response.body();
                if (userSession == null) {
                    error.postValue(UNKNOWN_ERROR);
                }
                else if (userSession.isLogged()) {
                    logged.postValue(true);
                }
                else error.postValue(userSession.getError());
                loading.postValue(false);
            }

            @Override
            public void onFailure(Call<UserSession> call, Throwable t) {
                error.postValue(t.getMessage());
                loading.postValue(false);
                t.printStackTrace();
            }
        });
    }

    public MutableLiveData<Boolean> getLogged() {
        return logged;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public MutableLiveData<String> getError() {
        return error;
    }
}
