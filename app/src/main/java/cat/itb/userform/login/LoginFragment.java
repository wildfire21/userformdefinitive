package cat.itb.userform.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;

public class LoginFragment extends Fragment {

    @BindView(R.id.usernameLogin)
    TextInputEditText usernameLogin;
    @BindView(R.id.passwordLogin)
    TextInputEditText passwordLogin;
    @BindView(R.id.usernameLayout)
    TextInputLayout usernameLayout;
    @BindView(R.id.passwordLayout)
    TextInputLayout passwordLayout;
    private LoginViewModel mViewModel;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        mViewModel.getLoading().observe(getViewLifecycleOwner(), this::onLoadingChanged);
        mViewModel.getError().observe(getViewLifecycleOwner(), this::onErrorFired);
        mViewModel.getLogged().observe(getViewLifecycleOwner(), this::onLogChanged);

    }

    private void onLogChanged(Boolean aBoolean) {
        if(aBoolean) {
            Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_userLoggedFragment);
        }
    }

    private void onErrorFired(String s) {
        Snackbar snackbar = Snackbar.make(getView(), s, BaseTransientBottomBar.LENGTH_SHORT);
        snackbar.show();
    }

    ProgressDialog dialog;
    private void onLoadingChanged(Boolean aBoolean) {
        if(aBoolean) {
            dialog = ProgressDialog.show(getContext(), "",
                    "Loading", true);
            dialog.show();
        } else {
            if(dialog != null)
                dialog.dismiss();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick({R.id.loginFragmentLoginButton, R.id.registerFragmentRegisterButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.loginFragmentLoginButton:
                if(!doLogin()) {
                    mViewModel.isLogged(usernameLogin.getEditableText().toString());
                }
                break;
            case R.id.registerFragmentRegisterButton:
                Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_registerFragment);
                break;
        }
    }

    private boolean doLogin() {
        usernameLayout.setError("");
        passwordLayout.setError("");
        boolean errors = false;
        if (usernameLogin.getText().toString().isEmpty()) {
            usernameLayout.setError("Username Required");
            errors = true;
        }
        if (passwordLogin.getText().toString().isEmpty()) {
            passwordLayout.setError("Password Required");
            errors = true;
        }
        return errors;
    }
}
