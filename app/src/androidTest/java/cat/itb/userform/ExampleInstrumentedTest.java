package cat.itb.userform;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasErrorText;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void correctLoginTest() {
        onView(withId(R.id.welcomeLoginButton)).perform(click());
        onView(withId(R.id.usernameLogin)).perform(replaceText("user1"));
        onView(withId(R.id.passwordLogin)).perform(replaceText("password1"));
        onView(withId(R.id.loginFragmentLoginButton)).perform(click());
        onView(withId(R.id.logged)).check(matches(isDisplayed()));

    }

    @Test
    public void voidLoginTest() {
        onView(withId(R.id.welcomeLoginButton)).perform(click());
        onView(withId(R.id.usernameLogin)).perform(replaceText(""));
        onView(withId(R.id.passwordLogin)).perform(replaceText(""));
        onView(withId(R.id.loginFragmentLoginButton)).perform(click());
        onView(withText("Username Required")).check(matches(isDisplayed()));
        onView(withText("Password Required")).check(matches(isDisplayed()));
    }

    @Test
    public void correctRegisterTest() {
        onView(withId(R.id.welcomeRegisterButton)).perform(click());
        onView(withId(R.id.username)).perform(replaceText("user1"));
        onView(withId(R.id.password)).perform(replaceText("password1"));
        onView(withId(R.id.repeat_password)).perform(replaceText("password1"));
        onView(withId(R.id.email)).perform(replaceText("a@a.com"));
        onView(withId(R.id.name)).perform(replaceText("name1"));
        onView(withId(R.id.surnames)).perform(replaceText("surname1"));
        onView(withId(R.id.birth_date)).perform(replaceText("02/19/2020"));
        onView(withId(R.id.gender)).perform(replaceText("male"));
        onView(withId(R.id.check_terms)).perform(scrollTo(), click());
        onView(withId(R.id.registerFragmentRegisterButton)).perform(scrollTo(), click());
        onView(withId(R.id.logged)).check(matches(isDisplayed()));

    }

    @Test
    public void voidRegisterTest() {
        onView(withId(R.id.welcomeRegisterButton)).perform(click());
        onView(withId(R.id.username)).perform(replaceText(""));
        onView(withId(R.id.password)).perform(replaceText(""));
        onView(withId(R.id.repeat_password)).perform(replaceText(""));
        onView(withId(R.id.email)).perform(replaceText(""));
        onView(withId(R.id.name)).perform(replaceText(""));
        onView(withId(R.id.surnames)).perform(replaceText(""));
        onView(withId(R.id.birth_date)).perform(replaceText(""));
        onView(withId(R.id.gender)).perform(replaceText(""));
        onView(withId(R.id.registerFragmentRegisterButton)).perform(scrollTo(), click());
        onView(withText("Username Required")).check(matches(isDisplayed()));
        onView(withText("Password Required")).check(matches(isDisplayed()));
        onView(withText("Repeat Required")).check(matches(isDisplayed()));
        onView(withText("Email Required")).check(matches(isDisplayed()));
        onView(withText("Name Required")).check(matches(isDisplayed()));
        onView(withText("Surnames Required")).check(matches(isDisplayed()));
        onView(withText("Birth date Required")).check(matches(isDisplayed()));
        onView(withText("Gender Required")).check(matches(isDisplayed()));
    }

    @Test
    public void navigationTest() {
        onView(withId(R.id.welcomeLoginButton)).perform(click());
        onView(withId(R.id.registerFragmentRegisterButton)).perform(click());
    }
}
